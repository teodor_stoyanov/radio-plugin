package com.theblackchair.spotlight.plugin.radio.consumer;

import com.theblackchair.commons.logging.FileLoggerFactory;
import com.theblackchair.commons.rm.ItemExecutionStatus;
import com.theblackchair.commons.rm.ReleaseManagement;
import com.theblackchair.commons.rm.ReleaseManagementAction;
import com.theblackchair.spotlight.plugin.radio.common.dto.Song;
import com.theblackchair.spotlight.plugin.radio.utils.logging.LevelLogging;
import com.theblackchair.spotlight.plugin.radio.utils.SNMPManager;
import com.theblackchair.spotlight.plugin.radio.utils.SNMPSongService;
import org.slf4j.Logger;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service("releaseManagementRadio")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ReleaseManagementRadio implements ReleaseManagement, LevelLogging {

    private static final String SNMP_ADDRESS = "udp:100.99.56.170/2001";

    private String deviceId;

    private Integer logLevel;

    private Logger logger;

    private SNMPManager snmpManager;

    private SNMPSongService snmpSongService;

    @Override
    public Map<String, Object> apply(Object dto, Map<String, Object> configuration, ReleaseManagementAction action) {
        init(configuration);
        Map<String, Object> pluginResponse = new HashMap<>();

        try {
            Song song = (Song) dto;
            ItemExecutionStatus itemExecutionStatus = ItemExecutionStatus.valueOf((String) configuration.get("itemExecutionStatus"));

            checkLogLevel("Preparing to execute RM request for song with name: " + song.getName(), LevelLogging.LOG_INFO)
                    .ifPresent(x -> logger.info(x));

            if (itemExecutionStatus.equals(ItemExecutionStatus.UPDATE_ONLY)) {
                checkLogLevel("Preparing to execute UPDATE_ONLY for song with name: " + song.getName(), LevelLogging.LOG_DETAILED_INFO)
                        .ifPresent(x -> logger.info(x));

                overrideFields(configuration, song);
                snmpSongService.changeSongProp(song);

                pluginResponse.put("messages", "Successfully executed RM package for object of type " + dto.getClass().getCanonicalName());
                pluginResponse.put("itemExists", "...");
                pluginResponse.put("isDependent", "...");
            } else {
                pluginResponse.put("messages", "Error: Only update RM request can be processed");
            }
        } catch (Exception e) {
            checkLogLevel("Unable to execute RM request: " + e.getMessage(),
                    LevelLogging.LOG_ERROR).ifPresent(x -> logger.error(x));
            checkLogLevel("Exception: ", LevelLogging.LOG_TRACE)
                    .ifPresent(x -> logger.error(x, e));
            pluginResponse.put("messages", "Error: Unable to execute RM request: " + e.getMessage());
        }
        return pluginResponse;
    }


    @Override
    public int getCurrentLogLevel() {
        return logLevel;
    }

    private void init(Map<String, Object> configuration) {
        if (deviceId == null) {
            deviceId = String.valueOf(configuration.get("deviceId"));
        }
        if (logger == null) {
            logger = FileLoggerFactory.getInstance(ReleaseManagementRadio.class.getName(), deviceId);
        }
        if (logLevel == null) {
            logLevel = Integer.parseInt(String.valueOf(configuration.get("logLevel")));
        }
        snmpManager = new SNMPManager(SNMP_ADDRESS);

        snmpSongService = new SNMPSongService(snmpManager);
    }

    private void overrideFields(Map<String, Object> configuration, Song song) {
        checkLogLevel("Attempting to override fields for song: " + song.getName() + ". Fields are " +
                new HashMap<>(configuration).toString(), LevelLogging.LOG_DETAILED_INFO)
                .ifPresent(x -> logger.info(x));
        if (configuration.containsKey("overriddenFields")) {
            Map<String, String> overriddenFields = (Map<String, String>) configuration.get("overriddenFields");
            if (overriddenFields.containsKey("name")) {
                song.setName(overriddenFields.get("name"));
            }
            if (overriddenFields.containsKey("artist")) {
                song.setArtist(overriddenFields.get("artist"));
            }
            if (overriddenFields.containsKey("album")) {
                song.setAlbum(overriddenFields.get("album"));
            }
            if (overriddenFields.containsKey("plays")) {
                song.setPlays(Long.parseLong(overriddenFields.get("plays")));
            }
        }
    }
}
