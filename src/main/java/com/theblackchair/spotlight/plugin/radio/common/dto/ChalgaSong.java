package com.theblackchair.spotlight.plugin.radio.common.dto;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ChalgaSong extends Song{

    public ChalgaSong(String name, String artist, String album, Long plays, String oid) {
        super(name, artist, album, plays, oid);
    }
}
