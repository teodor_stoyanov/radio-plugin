package com.theblackchair.spotlight.plugin.radio.utils;

import com.theblackchair.spotlight.plugin.radio.common.dto.ChalgaSong;
import com.theblackchair.spotlight.plugin.radio.common.dto.MetalSong;
import com.theblackchair.spotlight.plugin.radio.common.dto.PopSong;
import com.theblackchair.spotlight.plugin.radio.common.dto.Song;
import org.snmp4j.smi.OID;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SNMPSongService {
    private static final String BASE_ADDRESS = "1.3.6.1.4.2.0.1.";

    private static final String NAME = ".7.0";
    private static final String ARTIST = ".8.0";
    private static final String ALBUM = ".9.0";
    private static final String GENRE = ".10.0";
    private static final String PLAYS = ".11.0";
    private static final int OBJECTS_COUNT = 15;

    private static final String CHALGA_GENRE = "Chalga";
    private static final String POP_GENRE = "Pop";

    private final SNMPManager client;

    public SNMPSongService(SNMPManager SNMPManager) {
        this.client = SNMPManager;
    }

    public List<Song> getAndConvertSongs() throws IOException {
        List<Song> playlist = new ArrayList<>();

        client.start();

        String nameAddress;
        String artistAddress;
        String albumAddress;
        String genreAddress;
        String playsAddress;

        for (int i = 0; i < OBJECTS_COUNT; i++) {

            nameAddress = BASE_ADDRESS + i + NAME;
            artistAddress = BASE_ADDRESS + i + ARTIST;
            albumAddress = BASE_ADDRESS + i + ALBUM;
            genreAddress = BASE_ADDRESS + i + GENRE;
            playsAddress = BASE_ADDRESS + i + PLAYS;

            String name = client.getAsString(new OID(nameAddress));
            String artist = client.getAsString(new OID(artistAddress));
            String album = client.getAsString(new OID(albumAddress));
            String genre = client.getAsString(new OID(genreAddress));
            Long plays = Long.parseLong(client.getAsString(new OID(playsAddress)));

            Song song;

            switch (genre) {
                case CHALGA_GENRE:
                    song = new ChalgaSong(name, artist, album, plays, BASE_ADDRESS + i);
                    playlist.add(song);
                    break;
                case POP_GENRE:
                    song = new PopSong(name, artist, album, plays, BASE_ADDRESS + i);
                    playlist.add(song);
                    break;
                default:
                    song = new MetalSong(name, artist, album, plays, BASE_ADDRESS + i);
                    playlist.add(song);
                    break;
            }

        }
        client.closeSnmp();

        return playlist;
    }

    public void changeSongProp(Song song) throws IOException {
        client.start();
        //set Name
        client.set(new OID(song.getOid() + NAME), song.getName());
        //set Artist
        client.set(new OID(song.getOid() + ARTIST), song.getArtist());
        //set Album
        client.set(new OID(song.getOid() + ALBUM), song.getAlbum());
        //set Plays
        client.set(new OID(song.getOid() + PLAYS), song.getPlays().toString());

        client.closeSnmp();
    }
}
