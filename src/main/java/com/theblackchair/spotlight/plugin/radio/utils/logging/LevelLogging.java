package com.theblackchair.spotlight.plugin.radio.utils.logging;

import java.util.Optional;

public interface LevelLogging {
    int LOG_ERROR = 0;
    int LOG_INFO = 1;
    int LOG_DETAILED_INFO = 2;
    int LOG_TRACE = 3;

    /** Override this to return the log level you got from the config **/
    int getCurrentLogLevel();

    default Optional<String> checkLogLevel(String message, int level) {
        if (level > getCurrentLogLevel()) {
            return Optional.empty();
        }
        return Optional.of("[LOG_LEVEL_" + level + "] ### " + message + " ###");
    }
}
