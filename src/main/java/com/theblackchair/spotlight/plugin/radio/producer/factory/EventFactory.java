package com.theblackchair.spotlight.plugin.radio.producer.factory;


import com.theblackchair.commons.logging.trace.EventLog;
import com.theblackchair.commons.logging.trace.EventStatus;
import com.theblackchair.commons.logging.trace.LogService;
import com.theblackchair.commons.plugin.Actions;
import com.theblackchair.commons.plugin.Event;
import com.theblackchair.commons.plugin.IdList;
import com.theblackchair.spotlight.plugin.radio.common.dto.Song;
import org.springframework.lang.Nullable;

import java.util.Date;
import java.util.Map;

public class EventFactory {

    private LogService logService;
    public EventFactory(LogService logService) {
        this.logService = logService;
    }

    public Event create(String userId, IdList idList, String deviceId) {
        return create(userId, Actions.DELETE, idList, "IdList", deviceId, "IdList", null);
    }

    public Event create(String userId, Song song, String deviceId, String UID) {
        return create(userId, Actions.UPDATE,song,song.getName(), deviceId, UID, null);
    }

    private Event create(String userId, Actions action, Object object, String objectName, String deviceId, String UID, @Nullable Map<String, String> metadata) {
        //This part is used to generate the trace log info.
        Date startEventTime = new Date();
        String eventID = this.logService.generateID(deviceId, object.getClass().getCanonicalName(), System.nanoTime(), objectName);

        logService.logEvent(new EventLog.Builder(eventID, deviceId, EventStatus.CREATING)
                .setAction(action)
                .setObjectType(object.getClass().getCanonicalName())
                .setDate(startEventTime.getTime())
                .build());

        //Creation of the actual event.
        Event event = new Event();
        //Property, used when baselining/syncing. Set it to false.
        event.setLast(false);
        //Name of the user, who "created" the event. Shown in the Spotlight UI.
        event.setUserId(userId);
        //Date of the event. Either retrieve it from an argument or create a current date object here.
        event.setDate(startEventTime);
        //Audit log message, used in some specific plugins. Set it to an empty string.
        event.setAuditLogMessage("");
        //The event action (create/delete/update etc). Usually retrieved from an argument but can be set here also.
        event.setAction(action.toString());
        //The type of the object. Should be the canonical class name of the object. If sending JSON instead of an object,
        //it can be set to null.
        event.setObjectType(object.getClass().getCanonicalName());
        //The object that the event was made for. If sending JSON instead of an object, it can be set to null.
        event.setObject(object);
        //The "Spotlight name" of the object. Can be set to whatever you want the name to be in the Spotlight UI.
        event.setChangedValue(objectName);
        //The Device ID. It should be included in the plugin configuration.
        event.setDeviceId(deviceId);
        //The JSON that the event was made for. If you are not sending JSON, set it to null.
        event.setJson(null);
        //The unique identifier (key) of the object (only required if the plugin will have a Spotlight consumer side).
        event.setUID(UID);
        //The trace log event ID.
        event.setId(eventID);
        //The event can OPTIONALLY include metadata, which is created in the form of a String, String map. It will only
        //show up in MongoDB.
        event.setMetaData(metadata);
        return event;
    }
}
