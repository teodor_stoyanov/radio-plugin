package com.theblackchair.spotlight.plugin.radio.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class Song extends SkeletonDto {

    private String name;
    private String artist;
    private String album;
    private Long plays;
    private String oid;

    @Override
    public String toString() {
        return "Song{" +
                "name='" + name + '\'' +
                ", artist='" + artist + '\'' +
                ", album=" + album +
                ", plays=" + plays +
                '}';
    }
}
