package com.theblackchair.spotlight.plugin.radio.common.dto;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class MetalSong extends Song {
    public MetalSong(String name, String artist, String album, Long plays, String oid) {
        super(name, artist, album, plays, oid);
    }
}
