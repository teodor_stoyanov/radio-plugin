package com.theblackchair.spotlight.plugin.radio.utils;

import com.theblackchair.commons.logging.FileLoggerFactory;
import com.theblackchair.spotlight.plugin.radio.common.dto.Song;
import org.slf4j.Logger;

public class UIDGenerator {
    private Logger logger = FileLoggerFactory.getInstance(UIDGenerator.class.getName());
    private String deviceId;

    public UIDGenerator(String deviceId) {
        this.deviceId = deviceId;
    }

    private String getHash(String txt, String hashType) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance(hashType);
            byte[] array = md.digest(txt.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            logger.error("Exception while hashing: " + e.getMessage());
            //error action
        }
        return null;
    }

    private String md5(String txt) {
        return getHash(txt, "MD5");
    }

    private String sha1(String txt) {
        return getHash(txt, "SHA1");
    }

    public String generateUID(Song song) {
        return md5(song.getClass().getName() + song.getName() + song.getArtist() + deviceId);
    }
}
