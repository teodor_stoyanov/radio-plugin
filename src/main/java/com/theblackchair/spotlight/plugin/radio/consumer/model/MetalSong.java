package com.theblackchair.spotlight.plugin.radio.consumer.model;

import com.theblackchair.commons.persistence.entity.annotation.Template;

import javax.persistence.Entity;
import javax.persistence.Table;

@Template(name = "MetalSong")
@Entity
@Table(name = "CustomPluginMetalSong")
public class MetalSong extends Song{
}
