package com.theblackchair.spotlight.plugin.radio.consumer.model;

import com.theblackchair.commons.persistence.entity.SpotlightItemUniqueNameRelationParent;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.MappedSuperclass;

@Data
@NoArgsConstructor
@MappedSuperclass
public abstract class Song extends SpotlightItemUniqueNameRelationParent {
    private String name;
    private String artist;
    private String album;
    private Long plays;
    private String oid;
}
