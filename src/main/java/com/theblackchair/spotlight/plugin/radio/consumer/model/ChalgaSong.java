package com.theblackchair.spotlight.plugin.radio.consumer.model;

import com.theblackchair.commons.persistence.entity.annotation.Template;

import javax.persistence.Entity;
import javax.persistence.Table;

@Template(name = "ChalgaSong")
@Entity
@Table(name = "CustomPluginChalgaSong")
public class ChalgaSong extends Song {
}
