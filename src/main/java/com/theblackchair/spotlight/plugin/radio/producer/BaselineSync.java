package com.theblackchair.spotlight.plugin.radio.producer;

import com.theblackchair.commons.configuration.DeviceDTO;
import com.theblackchair.commons.filesharing.PluginStateFileShareUtils;
import com.theblackchair.commons.logging.FileLoggerFactory;
import com.theblackchair.commons.logging.trace.LogService;
import com.theblackchair.commons.plugin.Actions;
import com.theblackchair.commons.plugin.Emitter;
import com.theblackchair.commons.plugin.sync.ListBaseDeviceState;
import com.theblackchair.commons.plugin.sync.SyncMonitorService;
import com.theblackchair.spotlight.plugin.radio.common.dto.ChalgaSong;
import com.theblackchair.spotlight.plugin.radio.common.dto.MetalSong;
import com.theblackchair.spotlight.plugin.radio.common.dto.PopSong;
import com.theblackchair.spotlight.plugin.radio.common.dto.Song;
import com.theblackchair.spotlight.plugin.radio.producer.factory.EventFactory;
import com.theblackchair.spotlight.plugin.radio.utils.logging.LevelLogging;
import com.theblackchair.spotlight.plugin.radio.utils.SNMPSongService;
import com.theblackchair.spotlight.plugin.radio.utils.UIDGenerator;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BaselineSync extends ListBaseDeviceState<Song> implements LevelLogging {

    private UIDGenerator uidGenerator;

    private Logger logger;

    private PluginStateFileShareUtils pluginStateFileShareUtils;

    private String deviceId;

    private EventFactory eventFactory;

    private String userId;

    private SNMPSongService snmpSongService;

    private int logLevel;

    private boolean baseLineEnabled;

    private boolean areSupportedObjectsGiven;


    public BaselineSync(Emitter emitter, DeviceDTO deviceDTO, LogService logService, SyncMonitorService syncMonitorService, UIDGenerator uidGenerator,
                        PluginStateFileShareUtils pluginStateFileShareUtils, String deviceId,
                        EventFactory eventFactory, String userId, SNMPSongService snmpSongService, int logLevel) {
        super(emitter, deviceDTO, logService, syncMonitorService);
        this.uidGenerator = uidGenerator;
        this.logger = FileLoggerFactory.getInstance(BaselineSync.class.getName(), deviceId);
        this.pluginStateFileShareUtils = pluginStateFileShareUtils;
        this.deviceId = deviceId;
        this.eventFactory = eventFactory;
        this.userId = userId;
        this.snmpSongService = snmpSongService;
        this.logLevel = logLevel;
        this.baseLineEnabled = false;
        this.areSupportedObjectsGiven = false;
    }

    @Override
    protected List<Song> getAllObjectsPerType(String objectType) {
        checkLogLevel("Getting all objects of type " + objectType,
                LevelLogging.LOG_DETAILED_INFO).ifPresent(x -> logger.info(x));

        try {
            return snmpSongService.getAndConvertSongs().stream().filter(x -> x.getClass().getSimpleName().equals(objectType))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            checkLogLevel("An exception occurred in the plugin: "
                            + e.getClass().getSimpleName() + ": " + e.getMessage(),
                    LevelLogging.LOG_ERROR).ifPresent(x -> logger.error(x));
            checkLogLevel("Exception: ", LevelLogging.LOG_TRACE)
                    .ifPresent(x -> logger.error(x, e));
        }

        return new ArrayList<>();
    }

    @Override
    public void start(String syncId, List<String> list, Actions actions) {
        checkLogLevel("Starting baseline/sync...",
                LevelLogging.LOG_DETAILED_INFO).ifPresent(x -> logger.info(x));
        //Start the baseline/sync
        super.start(syncId, list, actions);
        this.baseLineEnabled = true;
        checkLogLevel("Finished baseline/sync",
                LevelLogging.LOG_DETAILED_INFO).ifPresent(x -> logger.info(x));
    }

    @Override
    protected boolean sendEventToQueue(Actions actions, String eventID, Song object) {
        String UID = uidGenerator.generateUID(object);

        checkLogLevel("Emitting baseline/sync event: " + object, LevelLogging.LOG_DETAILED_INFO)
                .ifPresent(x -> logger.info(x));
        super.emitEvent(eventFactory.create(userId, object, deviceId, UID));

        return true;
    }

    @Override
    public List<String> getSupportedObjects() {
        if(!this.areSupportedObjectsGiven){
            this.supportedObjects.add(ChalgaSong.class.getSimpleName());
            this.supportedObjects.add(MetalSong.class.getSimpleName());
            this.supportedObjects.add(PopSong.class.getSimpleName());
            this.areSupportedObjectsGiven = true;
        }
        checkLogLevel("List of supported objects: " + new ArrayList<>(supportedObjects).toString(),
                LevelLogging.LOG_DETAILED_INFO).ifPresent(x -> logger.info(x));
        return this.supportedObjects;
    }

    @Override
    protected String getEventId(Song object) {
        return logService.generateID(deviceId, object.getClass().getCanonicalName(), System.nanoTime(), object.getDocumentId());
    }

    @Override
    public int getCurrentLogLevel() {
        return logLevel;
    }

    public boolean isBaseLineEnabled() {
        return baseLineEnabled;
    }
}
