package com.theblackchair.spotlight.plugin.radio.producer.configuration;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RadioConfiguration {
    private int logLevel;
    private String username;
    private String snmpAddress;
}
