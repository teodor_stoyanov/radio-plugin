package com.theblackchair.spotlight.plugin.radio.common.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.theblackchair.commons.plugin.Document;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

public abstract class SkeletonDto implements Serializable, Document {
    @Id
    @JsonIgnore
    private String documentId;

    @Override
    public String getDocumentId() {
        return this.documentId;
    }

    @Override
    public void setDocumentId(String s) {
        this.documentId = s;
    }
}