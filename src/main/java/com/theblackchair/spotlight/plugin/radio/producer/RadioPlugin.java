package com.theblackchair.spotlight.plugin.radio.producer;

import com.theblackchair.commons.filesharing.PluginStateFileShareUtils;
import com.theblackchair.commons.logging.FileLoggerFactory;
import com.theblackchair.commons.plugin.ConfigurationServiceHostAware;
import com.theblackchair.commons.plugin.Event;
import com.theblackchair.commons.plugin.FactoryMapConfiguration;
import com.theblackchair.commons.plugin.PluginScheduler;
import com.theblackchair.commons.plugin.sync.SyncMonitorService;
import com.theblackchair.spotlight.plugin.radio.common.dto.Song;
import com.theblackchair.spotlight.plugin.radio.producer.configuration.RadioConfiguration;
import com.theblackchair.spotlight.plugin.radio.producer.factory.EventFactory;
import com.theblackchair.spotlight.plugin.radio.utils.logging.LevelLogging;
import com.theblackchair.spotlight.plugin.radio.utils.SNMPManager;
import com.theblackchair.spotlight.plugin.radio.utils.SNMPSongService;
import com.theblackchair.spotlight.plugin.radio.utils.UIDGenerator;
import org.slf4j.Logger;

import java.util.List;

public class RadioPlugin extends PluginScheduler implements ConfigurationServiceHostAware, LevelLogging {

    private PluginStateFileShareUtils pluginStateFileShareUtils;

    private List<String> configurationServiceHostUrls;

    private RadioConfiguration configuration;

    private Logger logger;

    private String deviceId;

    private boolean pluginActive;

    private SNMPManager snmpManager;

    private SNMPSongService snmpSongService;

    private UIDGenerator uidGenerator;

    private int logLevel;

    private EventFactory eventFactory;

    private BaselineSync baselineSync;

    @Override
    public void scanDevice() {
        try {
            if (pluginActive) {
                if (!baselineSync.isBaseLineEnabled()) {
                    checkLogLevel("The plugin will not start retrieving data until a Baseline/Sync is ran.", LevelLogging.LOG_ERROR)
                            .ifPresent(x -> logger.error(x));
                } else {
                    checkLogLevel("Attempting to retrieve objects form SNMP Server", LevelLogging.LOG_DETAILED_INFO)
                            .ifPresent(x -> logger.info(x));

                    List<Song> playlist = snmpSongService.getAndConvertSongs();

                    checkLogLevel("Finished parsing SNMP data", LevelLogging.LOG_DETAILED_INFO)
                            .ifPresent(x -> logger.info(x));

                    for (Song song : playlist) {
                        String UID = uidGenerator.generateUID(song);
                        Event event = eventFactory.create(configuration.getUsername(), song, deviceId, UID);
                        checkLogLevel("Emitting event: " + event.getObject(), LevelLogging.LOG_DETAILED_INFO)
                                .ifPresent(x -> logger.info(x));
                        emitter.emit(event);
                    }
                }
            }
        } catch (Exception e) {
            checkLogLevel("An exception occurred in the plugin: "
                            + e.getClass().getSimpleName() + ": " + e.getMessage(),
                    LevelLogging.LOG_ERROR).ifPresent(x -> logger.error(x));
            checkLogLevel("Exception: ", LevelLogging.LOG_TRACE)
                    .ifPresent(x -> logger.error(x, e));
        }
    }

    @Override
    public boolean activate() {
        deviceId = super.getDeviceDTO().getDeviceId();
        logger = FileLoggerFactory.getInstance(RadioPlugin.class.getName(), deviceId);
        configuration = new FactoryMapConfiguration().create(super.getDeviceDTO().getConfig(), RadioConfiguration.class);
        uidGenerator = new UIDGenerator(deviceId);
        eventFactory = new EventFactory(super.getTraceLogService());
        pluginStateFileShareUtils = new PluginStateFileShareUtils(deviceId, configurationServiceHostUrls);
        logLevel = configuration.getLogLevel();
        snmpManager = new SNMPManager(configuration.getSnmpAddress());
        snmpSongService = new SNMPSongService(snmpManager);
        checkLogLevel("Activating skeleton plugin with device id: " + deviceId, LevelLogging.LOG_INFO)
                .ifPresent(x -> logger.info(x));

        pluginActive = true;

        this.baselineSync = new BaselineSync(super.emitter,
                super.deviceDTO,
                super.traceLogService,
                new SyncMonitorService(super.publisher),
                uidGenerator,
                pluginStateFileShareUtils,
                deviceId,
                eventFactory,
                configuration.getUsername(),
                snmpSongService,
                logLevel);

        super.setDeviceState(this.baselineSync);

        setScheduleEmitting();

        return true;
    }

    @Override
    public boolean stop() {
        pluginActive = false;
        checkLogLevel("Stopped skeleton plugin with device id: " + deviceId, LevelLogging.LOG_INFO)
                .ifPresent(x -> logger.info(x));

        return true;
    }

    @Override
    public boolean start() {
        pluginActive = true;
        checkLogLevel("Resumed skeleton plugin with device id: " + deviceId, LevelLogging.LOG_INFO)
                .ifPresent(x -> logger.info(x));

        return true;
    }

    @Override
    public void setConfigurationServiceHostUrls(List<String> configurationServiceHostUrls) {
        this.configurationServiceHostUrls = configurationServiceHostUrls;
    }

    @Override
    public int getCurrentLogLevel() {
        return logLevel;
    }
}
