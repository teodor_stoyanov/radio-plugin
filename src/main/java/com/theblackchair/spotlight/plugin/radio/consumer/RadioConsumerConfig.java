package com.theblackchair.spotlight.plugin.radio.consumer;

import com.theblackchair.commons.configuration.ConfigurationValue;
import com.theblackchair.commons.plugin.ConfigurationProvider;

import java.util.HashMap;
import java.util.Map;

public class RadioConsumerConfig implements ConfigurationProvider {
    @Override
    public Map<String, Object> provideSpecificConfiguration() {
        Map<String, Object> config = new HashMap<>();
        config.put(ConfigurationValue.uniqueNameGenerator.name(), "v2");
        config.put(ConfigurationValue.mappingType.toString(), "relational");
        config.put("dataDictionaryEntryInitService", "radioDataDictionary.csv");
        config.put(ConfigurationValue.datadictionaryFile.toString(), "radioDataDictionary.csv");
        config.put(ConfigurationValue.modelPackage.toString(), "com.theblackchair.spotlight.plugin.radio.consumer.model");
        config.put("searchMetaDataFile", "radio-smd.csv");
        config.put("searchMetaDataServiceInitService", "radio-smd.csv");
        config.put("CPObjectTypeSeqFile", "radio-citdt.csv");
        config.put("CPObjectTypeSeqInitService", "radio-citdt.csv");
        config.put("sourceSystemFieldInitService", "radio-ssf.csv");
        return config;
    }
}
