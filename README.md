#IMPLEMENTING TEST PLUGIN

##Task 1 - Tracking Changes:
The song log should be polled for changes at a configurable interval, and whenever a poll is made, ONLY THE SONGS UPDATED SINCE THE LAST POLL should be retrieved.

Development steps:

1. Create your models and dtos

	- for dtos, all parent classes should extend SkeletonDto
	- parent models should extend SpotlightItemUniqueNameRelationParent
	- child models should extend SpotlightItemUniqueNameRelationChild and implement SpotLightObjectListChild if will be in a collection
2. Create a producer and consumer configurations
	- consumer configuration is a Map<String, Object> which can be populated with all necessary csv fles, model packages, ect.
	- producer configuration may be retrieved from configuration.json file and contain all static data for the plugin
3. Implement custom logic to connect with the device
	- SNMP libraries necessary for SNMP protocol and remote device connectivity
	- custom logic for SNMP GET (retrieve information)
	- convert that information to our dtos
4. Create the main plugin class which extends PluginSchedular
	- override all presented methods (start, stop, active , deviceScan, ect.)
	- create additional classes to support functionality (EventFactories , LevelLogger , UIDGenerator, ect.)
5. Generate the corresponding CSV files
	- DataDictionary.csv , smd.csv, citdt.csv
6. Create Environment and populate/change Configuration service configuration.json file

##Task 2 - Baseline:
Songs should be baselineable by genre (Chalga, Metal, Pop). Baselining a given genre should retrieve all songs for that genre. 
After baseline functionality is implemented, the plugin should not start to poll for changes until at least one baseline has been ran.

1. Create a Baseline class
    - extend ListBaseDeviceState<?> 
	- override methods
2. Set a flag for PluginSchedular not to start until a baseline is triggered with HTTP Post

##Task 3 - Release Management:
The only RM action that should be allowed is UPDATE (you cannot CREATE nor DELETE songs). All fields should be overridable,
 and executing an update should update the song data by executing the relevant SNMP SET requests.

1. Create a class implementing ReleaseManagement
	- override its methods 
2. Create ssf.csv file used for the RM
	- change override property for fields you want to be overridable
3. add relaseManagement object configuration in consumer configuration.json
4. Execute SQL script in slconfig database
	- PluginId should match column SourceSystem in ssf.csv file and envid should match your Environment ID
	- Example: 
	INSERT INTO slconfig.dbo.RMPlugins (PluginId, PluginDisplayName, Internal) VALUES('AvayaCMS', 'Avaya CMS', 0);
	INSERT INTO slconfig.dbo.RMPluginMethods (PluginId, MethodId, MethodURL) VALUES('AvayaCMS', 'execconfigitem', 'http://localhost:8081/rm/execconfigitem');
	INSERT INTO slconfig.dbo.RMPluginMethods (PluginId, MethodId, MethodURL) VALUES('AvayaCMS', 'verifyconfigitem', 'http://localhost:8081/rm/verifyconfigitem');
	INSERT INTO slconfig.dbo.EnvironmentKV (envid, envkey, envvalue) VALUES (6, 'ENV_PERM_ENABLECONFIGPACKAGE', 'true');
	INSERT INTO slconfig.dbo.EnvironmentKV (envid, envkey, envvalue) VALUES (6, 'ENV_PERM_ENABLEBASELINE', 'true');
5. In the Admin Panel of Spotlight UI under User section choose your Environment and profile, than enable Automation Package Execute.
